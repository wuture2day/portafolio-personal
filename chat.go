package main

import (
    "os"
	"fmt"
	"time"
	"math"
	"hash/fnv"
	"math/rand"
)

const (
    E   = 2.71828182845904523536028747135266249775724709369995957496696763
)
               // Declare object for create layers of neurons
type neuralLayer struct {
    nConn int
    nNeur int
    b[] float64
    w[][] float64
}

                             // DATASET
               // Convert string to binary hash
func stringToInt(s []string) (a [][]float64){
	var l int
	a = make([][]float64, len(s))
	k := make([]uint32, len(s))
	for i := 0; i < len(s); i++ {
	    h := fnv.New32a()
	    h.Write([]byte(s[i]))
	    k[i] = h.Sum32()
	    l = int(k[i])
	    for j := 0; j < 32; j++ {
	        if l%2 == 1 {
                   a[i] = append(a[i], 1.)
                } else {
                   a[i] = append(a[i], 0.)
                }
	        l = int(l/2)
	    }
	}
	return a
}
               // Finish conversion
                              // FINISH DATASET

                              // NEURAL NETWORK
               // Activation functions
func actF(nl []float64, f string, d bool) (a []float64) {
    switch f {
       case "sigm":
           a = sigm(nl, d)
       case "relu":
           a = relu(nl, d)
       case "step":
           a = step(nl, d)
       case "tanh":
           a = tanh(nl, d)
       case "mish":
           a = mish(nl, d)
       case "gaus":
           a = gaus(nl, d)
       case "sopl":
           a = sopl(nl, d)
       default:
           panic("funcion no declarada")
    }
    return a
}

func sigm(nl []float64, d bool) ([]float64) {
    a := make([]float64, 0)
    if d == false {
       for i := 0; i < len(nl); i++ {
           a = append(a, 1/(1+ math.Pow(E, - nl[i])))
       }//function
    } else {
       for i := 0; i < len(nl); i++ {
           a = append(a, nl[i]*(1-nl[i]))
       }//derivative
    }
    return a
}
func relu(nl []float64, d bool) ([]float64) {
    a := make([]float64, 0)
    if d == false {
       for i := 0; i < len(nl); i++ {
           if nl[i] < 0. {
              a = append(a, 1.)
           } else {
              a = append(a, 0.)
           }
       }//function
    } else {
       for i := 0; i < len(nl); i++ {
           if nl[i] > 0. {
              a = append(a, 1.)
           } else {
              a = append(a, 0.)
           }
       }//derivative
    }
    return a
}
func step(nl []float64, d bool) ([]float64) {
    a := make([]float64, 0)
    if d == false {
       for i := 0; i < len(nl); i++ {
           if nl[i] >= 0. {
              a = append(a, 1.)
           } else {
              a = append(a, 0.)
           }
       }//function
    } else {
       for i := 0; i < len(nl); i++ {
           if nl[i] == 0. {
              a = append(a, 1.)
           } else {
              a = append(a, 0.)
           }
       }//derivative
    }
    return a
}
func tanh(nl []float64, d bool) ([]float64) {
    a := make([]float64, 0)
    if d == false {
       for i := 0; i < len(nl); i++ {
           a = append(a, math.Tanh(nl[i]))
       }//function
    } else {
       for i := 0; i < len(nl); i++ {
           a = append(a, 1 - math.Pow((math.Tanh(nl[i])), 2))
       }//derivative
    }
    return a
}
func mish(nl []float64, d bool) ([]float64) {
    a := make([]float64, 0)
    if d == false {
       for i := 0; i < len(nl); i++ {
           a = append(a, nl[i]*math.Tanh(math.Log(1 + math.Pow(E, nl[i]))))
       }//function
    } else {
       for i := 0; i < len(nl); i++ {
           a = append(a, (math.Pow(E, nl[i])*(math.Pow(4*E, 2*nl[i]) + math.Pow(E, 3*nl[i]) + 4*(1 + nl[i]) + math.Pow(E, nl[i])*(6 + 4*nl[i])))/math.Pow((2 + math.Pow(2*E, nl[i]) + math.Pow(E, 2*nl[i])), 2))
       }//derivative
    }
    return a
}
func gaus(nl []float64, d bool) ([]float64) {
    a := make([]float64, 0)
    if d == false {
       for i := 0; i < len(nl); i++ {
           a = append(a, math.Pow(E, math.Pow(-nl[i], 2)))
       }//function
    } else {
       for i := 0; i < len(nl); i++ {
           a = append(a, -2*nl[i]*math.Pow(E, math.Pow(-nl[i], 2)))
       }//derivative
    }
    return a
}
func sopl(nl []float64, d bool) ([]float64) {
    a := make([]float64, 0)
    if d == false {
       for i := 0; i < len(nl); i++ {
           a = append(a, math.Log(1 + math.Pow(E, nl[i])))
       }//function
    } else {
       for i := 0; i < len(nl); i++ { // for k, e := range x {
           a = append(a, 1/(1 + math.Pow(E, -nl[i])))
       }//derivative
    }
    return a
}
               // Finish activation functions
               // Create object
func creN(topolog []int, s[][]string) ([]neuralLayer) {
    a := make([]neuralLayer, len(topolog)-1)
    var b neuralLayer
    for i := 0; i < len(topolog)-1; i++ {
        b = neuralLayer{nConn: topolog[i], nNeur: topolog[i+1],}
        a[i] = neuL(b, s)
    }
    return a
}

func neuL(nL neuralLayer, s[][]string) (neuralLayer) {
    rand.Seed(time.Now().UnixNano())
    nL.b = make([]float64, nL.nNeur)
    nL.w = make([][]float64, nL.nConn)
    k := []float64{0}
    
    for i := 0; i < nL.nNeur; i++ {
        for _, value := range rand.Perm(2) {
                k[0] = float64(value) * 2. -1.
        }
        nL.b[i] = k[0]
    }

    for i := 0; i < nL.nConn; i++ { 
        for j := 0; j < nL.nNeur; j++ {
            for _, value := range rand.Perm(2) {
                k[0] = float64(value)
            }
            if k[0] == 1. {
               nL.w[i] = append(nL.w[i], k[0])
            } else {
               nL.w[i] = append(nL.w[i], -1.)
            }
        }
    }

    return nL
}
               // Finish object

                             // FINISH NEURAL NETWORK

                             // TRAINING

func train(x [][][]float64, a []neuralLayer, d []string) {
    //for i:=0; i<len(x); i++ {
    //    var out [][]float64 {{}, {x[i]}}
    //}
    out := make([][][]float64, 0)
          b := make([]float64, len(x[0])) //w.[i]
          z := make([][]float64, 0)
          z = append(z, b[:])
     
          out = append(out, x[0])//i
          out = append(out, x[0])//i
          for j:=0; j<len(x[0]); j++ {//i
              for k := 0; k<len(a[0].w[j]); k++ {
                  sum := 0.
                  for l:=0; l<len(a[0].w); l++ {
                      sum += out[0][k][l] * a[0].w[l][j]
                  }
                  z[k][j] = append(z[k][j], sum)
              }
          }
    fmt.Println(out)
    fmt.Println(z)




    //z[j] = sum + a[i].b[j]
}
                             // FINISH TRAINING






                             

func main() {
                             // Create dataset
        s := [][]string{
          {"hola", "cómo estás?", "buenos días", "buenas tardes", "buenas noches"},
          {"la comida estuvo excelente", "me encantó la comida", "la comida estuvo de lujo", "que comida tan buena", "genial la comida"},
          {"no me gustó la comida","la comida estuvo horrible","que comida tan fea","que asco de comida","la comida estuvo espantosa"},
          {"quiero ordenar pizza","por favor quiero una pizza","pizza por favor","quiero ordenar hamburguesa","por favor quiero una hamburguesa","hamburguesa por favor","quiero ordenar ensalada","por favor quiero una ensalada","ensalada por favor","quiero ordenar gaseosa","por favor quiero una gaseosa","gaseosa por favor"},
          {"quiero ordenar pizza","por favor quiero una pizza","pizza por favor","quiero ordenar hamburguesa","por favor quiero una hamburguesa","hamburguesa por favor","quiero ordenar ensalada","por favor quiero una ensalada","ensalada por favor","quiero ordenar gaseosa","por favor quiero una gaseosa","gaseosa por favor"},
          {"quiero ordenar pizza","por favor quiero una pizza","pizza por favor"},
          {"quiero ordenar hamburguesa","por favor quiero una hamburguesa","hamburguesa por favor"},
          {"quiero ordenar ensalada","por favor quiero una ensalada","ensalada por favor"},
          {"quiero ordenar gaseosa","por favor quiero una gaseosa","gaseosa por favor"},
        }
        t := []string{"greeting","liked","disliked","food","order","pizza", "hamburger", "salad", "soda"}
        var X[][][]float64
        var Z[][]float64
        for i := 0; i < len(s); i++ {
            Z = stringToInt(s[i])
            X = append(X, Z)
        }
        Y := stringToInt(t)
        
        
        p := 32
                             // Finish dataset




                             // Create Neural Network
        topolog := []int{1, p, 8, 16, 9, len(t)}
        activationfunction := []string{"sigm", "relu", "sigm", "relu", "sigm", "relu"}
        neuralNet := creN(topolog, s)
        
        fmt.Printf("%T\n\n", Y[0])
        train(X, neuralNet, activationfunction)

}
